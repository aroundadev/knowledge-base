import React, {Component} from "react";
import {Link} from "react-router-dom";
import ClickOutHandler from "react-onclickout";
import PropTypes from "prop-types";

import Dots from "../../../assets/icons/dots-black.svg";

class TableRow extends Component {
    constructor(props) {
        super(props);
        this.changeState = this.changeState.bind(this);
        this.handleClick = this.handleClick.bind(this);  
        this.handleDelete = this.handleDelete.bind(this);     

        this.state = {
            groupMenuOpen: false,
            actionMenuOpen: false
        };
    }

    changeState(property) {
        this.setState({
            [property]: !this.state[property]
        });
    }

    handleClick(data) {
        this.props.updateUserData(this.props.id, data);
    }

    handleDelete() {
        this.props.toggleDeleteModal(this.props.id, this.props.name);
    }

    render() {
        const {name, email, id, groupTitle, restOfGroups} = this.props;
        return (
            <tr className="settings_section_table_row">
                <td>{name}</td>
                <td>{email}</td>
                <td
                    className="u-mng_menu_trigger"
                    onClick={() => this.changeState("groupMenuOpen")}
                >
                    {groupTitle}
                    {this.state.groupMenuOpen && (
                        <ClickOutHandler onClickOut={() => this.changeState("groupMenuOpen")}>
                            <ul
                                className={`u-mng_menu_list ${
                                    this.state.groupMenuOpen ? "-open" : ""
                                }`}
                            >
                                {restOfGroups.map(group => (
                                    <li key={group.id} onClick={() => this.handleClick({group_id: group.id})}>{group.title}</li>
                                ))}
                            </ul>
                        </ClickOutHandler>
                    )}
                </td>
                <td
                    className="u-mng_menu_trigger"
                    onClick={() => this.changeState("actionMenuOpen")}
                >
                    <img className="u-mng_actions_icon" src={Dots} alt="user actions icon" />
                    {this.state.actionMenuOpen && (
                        <ClickOutHandler onClickOut={() => this.changeState("actionMenuOpen")}>
                            <ul
                                className={`u-mng_menu_list ${
                                    this.state.actionMenuOpen ? "-open" : ""
                                }`}
                            >
                                <Link to={`/settings/edit-profile/${id}`}><li>Edit</li></Link>
                                <li onClick={() => this.handleDelete()}>Delete</li>
                            </ul>
                        </ClickOutHandler>
                    )}
                </td>
            </tr>
        );
    }
}

TableRow.propTypes = {
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    groupTitle: PropTypes.string.isRequired,
    restOfGroups: PropTypes.arrayOf(PropTypes.object),
    updateUserData: PropTypes.func.isRequired,
    toggleDeleteModal: PropTypes.func.isRequired
}

export default TableRow;
