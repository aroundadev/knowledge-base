import React from "react";
import PropTypes from "prop-types";


import Modal from "../Modal/Modal";

const StatusModal = ({toggleModal, status}) => {
    let statusText = "Nothing is happening";
    switch (status) {
    case "idle": {
        statusText = "Nothing is happening";
        break;
    }
    case "creating": {
        statusText = "The user is being added...";
        break;
    }
    case "success": {
        statusText = "New user is added!";
        break;
    }
    case "error": {
        statusText = "Something went wrong in the process!";
        break;
    }
    }

    return (
        <Modal toggleModal={() => status !== "creating" ? toggleModal() : false}>
            <div className="modal">
                <h3 className="modal_title">{statusText}</h3>
                {status !== "creating" && (
                    <button
                        type="button"
                        className="btn-general btn-confirm-visible modal_btn"
                        onClick={() => toggleModal()}
                    >
                        <p>Ok</p>
                    </button>
                )}
            </div>
        </Modal>
    );
};

StatusModal.propTypes = {
    toggleModal: PropTypes.func.isRequired,
    status: PropTypes.oneOf(["idle", "creating", "success", "error"]).isRequired
}

export default StatusModal;
