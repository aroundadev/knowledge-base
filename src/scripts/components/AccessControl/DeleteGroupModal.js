import React from "react";
import PropTypes from "prop-types";

import Modal from "../Modal/Modal";

const DeleteGroupModal = ({title, toggleModal, deleteGroup}) => (
    <Modal toggleModal={() => toggleModal("deleteGroupModal")}>
        <div className="modal">
            <h3 className="modal_title">Are you sure that you that you want to delete {title}?</h3>
            <button type="button" className="btn-general btn-cancel modal_btn"
                onClick={() => toggleModal("deleteGroupModal")}><p>No</p></button>
            <button type="button" className="btn-general btn-confirm modal_btn"
                onClick={() => deleteGroup()}><p>Yes</p></button>
        </div>
    </Modal>
);

DeleteGroupModal.propTypes = {
    groupId: PropTypes.string.isRequired,
    title: PropTypes.string,
    toggleModal: PropTypes.func.isRequired,
    deleteGroup: PropTypes.func.isRequired
}

export default DeleteGroupModal;