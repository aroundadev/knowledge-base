import React from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

import EditIcon from "../../../assets/icons/edit-icon.svg";
import CrossRemove from "../../../assets/icons/cross-remove.svg";

const Card = ({title, id, group, toggleDeleteModal, toggleEditModal, admin, readable}) => {
    if (!readable) {
        return (
            <div className="card -unreadable">
                <h1 className="card_title">{title}</h1>
                <h4 className="card_group">{group}</h4>
            </div>
        );
    }

    return (
        <Link to={`/article/${id}`}>
            <div className="card">
                {admin && (
                    <div className="card_actions_wrap">
                        <img
                            className="card_action"
                            src={EditIcon}
                            alt="Edit Card"
                            onClick={e => {
                                e.preventDefault();
                                return toggleEditModal();
                            }}
                        />
                        <img
                            className="card_action"
                            src={CrossRemove}
                            alt="Remove Card"
                            onClick={e => {
                                e.preventDefault();
                                return toggleDeleteModal();
                            }}
                        />
                    </div>
                )}
                <h1 className="card_title">{title}</h1>
                <h4 className="card_group">{group}</h4>
            </div>
        </Link>
    );
};

Card.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    group: PropTypes.string.isRequired,
    toggleDeleteModal: PropTypes.func.isRequired,
    toggleEditModal: PropTypes.func.isRequired,
    admin: PropTypes.bool.isRequired,
    readable: PropTypes.bool.isRequired
}

export default Card;
