const admin = require("firebase-admin");

const checkIfAdmin = (req, res, next) => {
    const token = req.get("X-User-Token");

    const checkUserGroupPermissions = group_id => {
        admin
            .firestore()
            .collection("userGroups")
            .doc(group_id)
            .get()
            .then(doc => {
                if (doc.exists) {
                    const permissions = doc.data().permissions;
                    if (permissions.admin) {
                        next();
                    }
                } else {
                    res.status(401).send("Unauthorised request");
                }
            })
            .catch(() => {
                res.status(401).send("Unauthorised request");
            });
    };

    const getUserGroupId = uid => {
        admin
            .firestore()
            .collection("users")
            .doc(uid)
            .get()
            .then(doc => {
                if (doc.exists) {
                    const group_id = doc.data().group_id;
                    checkUserGroupPermissions(group_id);
                } else {
                    res.status(401).send("Unauthorised request");
                }
            })
            .catch(() => {
                res.status(401).send("Unauthorised request");
            });
    };

    admin
        .auth()
        .verifyIdToken(token)
        .then(decodedToken => {
            const uid = decodedToken.uid;
            getUserGroupId(uid);
        })
        .catch(() => {
            res.status(401).send("Unauthorised request");
        });
};

module.exports = {
    checkIfAdmin
};
