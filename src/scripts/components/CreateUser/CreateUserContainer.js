import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";

import CreateUser from "./CreateUser.js";

const mapStateToProps = state => ({
    userGroups: state.firestore.data.userGroups ? state.firestore.data.userGroups : {}
});

const CreateUserContainer = compose(
    firestoreConnect(() => [
        {collection: "userGroups", storeAs: "userGroups"}
    ]),
    connect(mapStateToProps)
)(CreateUser);

export default CreateUserContainer;