import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";
import PropTypes from "prop-types";

import Article from "./Article";


const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    let currentUserGroup = state.firestore.data.currentUserGroup;

    if(currentUserGroup) {
        const permissions = currentUserGroup.permissions;
        const cantEditArray = currentUserGroup["cant-edit"] || [];
        const cantReadArray = currentUserGroup["cant-read"] || [];        


        const editable = permissions.edit && cantEditArray.indexOf(id) === -1;
        const readable = permissions.read && cantReadArray.indexOf(id) === -1;

        return {
            article: state.firestore.data[`article_${id}`] && state.firestore.data[`article_${id}`],
            editable,
            readable
        };
    }
    else {
        return {
            article: state.firestore.data[`article_${id}`] && state.firestore.data[`article_${id}`],
            readable: true
        };
    }
    
};

const ArticleContainer = compose(
    firestoreConnect(props => {
        const id = props.match.params.id;

        return([
            {collection: "cardArticles", doc: id, storeAs: `article_${id}`},           
        ])
    }),
    connect(mapStateToProps)
)(Article);

ArticleContainer.propTypes = {
    match: PropTypes.object.isRequired
}

export default ArticleContainer;
