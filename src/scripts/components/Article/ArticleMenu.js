import React from "react";
import Scrollchor from "react-scrollchor";
import PropTypes from "prop-types";

const ArticleMenu = ({title, structure}) => {

    if (!structure) {
        return (
            <div className="page_menu">
                <div className="page_menu_content">
                    <h3 className="page_menu_title">{title}</h3>
                    <ol className="page_menu_list article_menu_list">
                    </ol>
                </div>
            </div>
        );
    }
    const sections = Object.keys(structure);
    let listItems = sections.map((section, index) => {
        const sectionName = section.replace(/\d__/, "");

        return (
            <li key={index} className="page_menu_list_section article_menu_list_section"><Scrollchor
                to={`#${index}_${sectionName}`}>{sectionName}</Scrollchor>
            {structure[section].length > 0 &&
                <ul className="page_menu_list_sublist">
                    {
                        structure[section].map((subsection, index) => {
                            const subsectionName = subsection.replace(/\d__/, "");

                            return (
                                <li key={index} className="page_menu_list_subsection">
                                    <Scrollchor to={`#${index}_${subsectionName}`}>{subsectionName}</Scrollchor></li>
                            );
                        })
                    }
                </ul>
            }
            </li>
        );
    });

    return (
        <div className="page_menu">
            <div className="page_menu_content">
                <h3 className="page_menu_title">{title}</h3>
                <ol className="page_menu_list article_menu_list">
                    {listItems}
                </ol>
            </div>
        </div>
    );
};

ArticleMenu.propTypes = {
    title: PropTypes.string,
    structure: PropTypes.object
}

export default ArticleMenu;