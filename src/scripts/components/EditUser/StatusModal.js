import React from "react";
import PropTypes from "prop-types";

import Modal from "../Modal/Modal";

const StatusModal = ({toggleModal, status}) => {
    let statusText = "Nothing is happening";
    switch (status) {
    case "idle": {
        statusText = "Nothing is happening";
        break;
    }
    case "editing": {
        statusText = "The user is being edited...";
        break;
    }
    case "success": {
        statusText = "The user is editted!";
        break;
    }
    case "error": {
        statusText = "Something went wrong in the process!";
        break;
    }
    }

    return (
        <Modal toggleModal={() => status !== "editing" ? toggleModal() : false}>
            <div className="modal">
                <h3 className="modal_title">{statusText}</h3>
                {status !== "editing" && (
                    <button
                        type="button"
                        className="btn-general btn-confirm-visible modal_btn"
                        onClick={() => toggleModal()}
                    >
                        <p>Ok</p>
                    </button>
                )}
            </div>
        </Modal>
    );
};

StatusModal.propTypes = {
    toggleModal: PropTypes.func.isRequired,
    status: PropTypes.oneOf(["idle", "editing", "success", "error"]).isRequired
}

export default StatusModal;
