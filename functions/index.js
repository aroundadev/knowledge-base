const functions = require("firebase-functions");
const admin = require("firebase-admin");

const appCredentials = require("./appCredentials");
const api = require("./api");

admin.initializeApp({
    credential: admin.credential.cert(appCredentials),
    databaseURL: "https://arounda-kb.firebaseio.com"
});

exports.reassignDeletedUserGroup = functions.firestore.document("userGroups/{userGroupId}").onDelete(event => {
    const groupId = event.params.userGroupId;

    const setCurrGroupToUnassigned = (userId) => {
        const unassignedId = "G6AEPJADW1reAtb3YMb1";
        admin.firestore().collection("users").doc(userId).update({
            group_id: unassignedId
        });
    }

    admin.firestore().collection("users").where("group_id", "==", groupId).get().then(snapshot => {
        snapshot.forEach(doc => {
            setCurrGroupToUnassigned(doc.id);
        })
    }).catch(error => console.log(error));
})

exports.api = functions.https.onRequest((req, res) => {
    api(req,res);
})
