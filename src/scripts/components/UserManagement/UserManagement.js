import React, {Component} from "react";
import {Link} from "react-router-dom";
import noScroll from "no-scroll";
import {isLoaded} from "react-redux-firebase";
import axios from "axios";
import PropTypes from "prop-types";

import TableRow from "./TableRow";
import DeleteUserModal from "./DeleteUserModal";
import {dataObjectToArray} from "../../tools/toolFunctions";

import CrossAddWhite from "../../../assets/icons/cross-add-white.svg";

class UserManagement extends Component {
    constructor(props) {
        super(props);
        this.updateUserData = this.updateUserData.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);

        this.state = {
            deleteModalOpen: false,
            status: "idle"
        };
    }

    updateUserData(userId, data) {
        this.props.firestore.update(`users/${userId}`, data);
    }

    deleteUser(userId) {
        const apiURL = `https://us-central1-arounda-kb.cloudfunctions.net/api/user?id=${userId}`;

        this.setState(
            {
                status: "deleting"
            },
            () => {
                this.props.firebase
                    .auth()
                    .currentUser.getIdToken()
                    .then(token => {
                        axios
                            .delete(apiURL, {
                                headers: {"X-User-Token": token}
                            })
                            .then(() => {
                                this.setState({
                                    status: "success"
                                });
                            })
                            .catch(() => {
                                this.setState({
                                    status: "error"
                                });
                            });
                    });
            }
        );
    }

    toggleDeleteModal(userId, userName) {
        noScroll.toggle();

        if (!this.state.deleteModalOpen) {
            this.setState({
                userToDelete: {id: userId, name: userName},
                deleteModalOpen: true
            });
        } else {
            this.setState({
                deleteModalOpen: false,
                status: "idle"
            });
        }
    }

    render() {
        if (!isLoaded(this.props.users, this.props.userGroups)) {
            return (
                <section className="settings_section">
                    <div className="settings_section_head">
                        <h1 className="settings_section_head_title">Users</h1>
                    </div>
                    Loading
                </section>
            );
        }
        let users = dataObjectToArray(this.props.users),
            groups = dataObjectToArray(this.props.userGroups);
        return (
            <section className="settings_section">
                <div className="settings_section_head">
                    <h1 className="settings_section_head_title">Users</h1>
                    <Link
                        className="settings_section_head_link u-mng_add-link"
                        to="/settings/create-user"
                    >
                        <img src={CrossAddWhite} alt="" width="10" height="10" />
                        Add
                    </Link>
                </div>
                <table className="settings_section_table">
                    <thead>
                        <tr className="settings_section_table_row">
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Groups</th>
                            <th />
                        </tr>
                    </thead>

                    <tbody>
                        {users.map(singleUser => {
                            const user = singleUser;
                            const groupArr = groups.filter(group => {
                                return group.id === user.group_id;
                            })[0];

                            let group = groupArr
                                ? groupArr
                                : Object.assign({}, this.props.userGroups.G6AEPJADW1reAtb3YMb1, {
                                    id: "G6AEPJADW1reAtb3YMb1"
                                });

                            const restOfGroups = groups.filter(group => {
                                return group.id !== user.group_id;
                            });

                            return (
                                <TableRow
                                    key={user.id}
                                    name={user.name}
                                    email={user.email}
                                    id={user.id}
                                    groupTitle={group.title}
                                    restOfGroups={restOfGroups}
                                    updateUserData={this.updateUserData}
                                    toggleDeleteModal={this.toggleDeleteModal}
                                />
                            );
                        })}
                    </tbody>
                </table>
                {this.state.deleteModalOpen && (
                    <DeleteUserModal
                        visible={this.state.deleteModalOpen}
                        userId={this.state.userToDelete.id}
                        userName={this.state.userToDelete.name}
                        toggleModal={this.toggleDeleteModal}
                        deleteUser={this.deleteUser}
                        status={this.state.status}
                    />
                )}
            </section>
        );
    }
}

UserManagement.propTypes = {
    firestore: PropTypes.object.isRequired,
    firebase: PropTypes.object.isRequired,
    users: PropTypes.object,
    userGroups: PropTypes.object
}

export default UserManagement;
