import React, {Component} from "react";
import {firestoreConnect} from "react-redux-firebase";
import PropTypes from "prop-types";

import InputGeneral from "../General/InputGeneral";

const Logo = require("../../../assets/icons/logotype.svg");

class Restore extends Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            email: "",
            resetIsInitiated: false,
            emailError: false
        };
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.firebase
            .resetPassword(this.state.email)
            .then(() => this.setState({resetIsInitiated: true}))
            .catch(error => {
                switch (error.code) {
                case "auth/invalid-email": {
                    this.setState({
                        emailError: true
                    });
                    break;
                }
                case "auth/user-not-found": {
                    this.setState({
                        emailError: true
                    });
                    break;
                }
                default: {
                    this.setState({
                        emailError: true
                    });
                    break;
                }
                }
            });
    }

    onInputChange(e) {
        const name = e.target.name;

        let newState = {};
        newState[name] = e.target.value;

        this.setState(newState);
    }

    showContent() {
        if (this.state.resetIsInitiated) {
            return (
                <div className="login_container">
                    <h1 className="login_heading">Email is sent</h1>
                    <p className="login_text">
                        Email with link to change your password is sent to email you have provided
                    </p>
                </div>
            );
        }

        return (
            <div className="login_container">
                <h1 className="login_heading">Restore password</h1>
                <p className="login_text">
                    Enter the mail address associated with your account, and we`ll send you a link
                    to reset your password
                </p>
                <form className="login_form" onSubmit={e => this.handleSubmit(e)}>
                    <InputGeneral
                        settings={{
                            placeholder: "Email",
                            type: "email",
                            name: "email",
                            required: true
                        }}
                        valueState={this.state.email}
                        errorState={this.state.emailError}
                        errorText="No user with such email"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <button
                        type="submit"
                        className="btn-general btn-confirm-visible login_btn-confirm"
                    >
                        Login
                    </button>
                </form>
            </div>
        );
    }

    render() {
        return (
            <div className="cont-zone">
                <div className="big-header">
                    <img className="big-header_logo" src={Logo} alt="Arounda Agency" />
                </div>
                {this.showContent()}
            </div>
        );
    }
}

Restore.contextTypes = {
    router: PropTypes.object.isRequired
};

Restore.propTypes = {
    firebase: PropTypes.object.isRequired
}

export default firestoreConnect()(Restore);
