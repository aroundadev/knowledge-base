import React, {Component} from "react";
import {isLoaded} from "react-redux-firebase";
import {Route, Redirect} from "react-router-dom";
import PropTypes from "prop-types";

import ArticleMenu from "./ArticleMenu";
import HeaderMenu from "../HeaderMenu";
import ArticleEditor from "./ArticleEditor";
import ContentParser from "./ContentParser";
import ArticleContent from "./ArticleContent";

class Article extends Component {
    constructor(props) {
        super(props);
        this.updateArticleContent = this.updateArticleContent.bind(this);
    }

    updateArticleContent(content) {
        const newArticleContent = ContentParser.parseNewContent(content);
        this.props.firestore
            .update(`cardArticles/${this.props.article.card_id}`, newArticleContent)
            .then(() => {
                this.props.history.goBack();
            });
    }

    render() {
        let structure = this.props.article ? this.props.article.structure : {};
        let content = this.props.article ? this.props.article.content : undefined;

        if (!isLoaded(this.props.article)) {
            return <div>Loading</div>;
        }

        if (!this.props.readable) {
            return <Redirect to="/" />;
        }

        return (
            <div className="cont-zone cont-zone-page">
                <div className="page">
                    <ArticleMenu title={this.props.article.card_title} structure={structure} />
                    <div className="page_content_wrap">
                        <div className="page_upper-row">
                            <button
                                className="page_back-btn"
                                onClick={() => this.props.history.push("/")}
                            >
                                To All Cards
                            </button>
                            <HeaderMenu />
                        </div>
                        <Route
                            exact
                            path="/article/:id"
                            render={() => (
                                <ArticleContent
                                    editable={this.props.editable}
                                    content={content}
                                    id={this.props.article.card_id}
                                />
                            )}
                        />
                        {this.props.editable && (
                            <Route
                                path="/article/:id/edit"
                                render={() => (
                                    <ArticleEditor
                                        handleArticleUpdate={this.updateArticleContent}
                                        content={content}
                                    />
                                )}
                            />
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

Article.propTypes = {
    history: PropTypes.object.isRequired,
    firestore: PropTypes.object.isRequired,
    article: PropTypes.object,
    readable: PropTypes.bool,
    editable: PropTypes.bool
}

export default Article;
