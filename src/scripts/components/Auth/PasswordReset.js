import React, {Component} from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const Logo = require("../../../assets/icons/logotype.svg");

class PasswordReset extends Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            codeIsValid: false,
            passwordIsChanged: false,
            password: ""
        };
    }

    componentWillMount() {
        this.props.verifyResetCode(this.props.resetCode).then(() => this.setState({codeIsValid: true}));
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.confirmPasswordReset(this.props.resetCode, this.state.password)
            .then(() => this.setState({passwordIsChanged: true}));
    }

    onInputChange(e) {
        const name = e.target.name;

        let newState = {};
        newState[name] = e.target.value;

        this.setState(newState);
    }

    showContent() {
        if(!this.state.codeIsValid) {
            return(
                <div className="login_container">
                    <h1 className="login_heading">Error</h1>
                    <p className="login_text">Password reset code is not valid or expired</p>
                    <Link to="/" className="btn-general btn-confirm-visible login_btn-confirm">Back To Login</Link>
                </div>
            );
        }

        if(this.state.passwordIsChanged) {
            return(
                <div className="login_container">
                    <h1 className="login_heading">Success!</h1>
                    <p className="login_text">You have change your password. Now you can login with your new credentials</p>
                    <Link to="/" className="btn-general btn-confirm-visible login_btn-confirm">To Login</Link>
                </div>
            );
        }

        return(
            <div className="login_container">
                <h1 className="login_heading">Enter your new password</h1>
                <form className="login_form" onSubmit={(e) => this.handleSubmit(e)}>
                    <input required placeholder="Password" name="password" type="password"
                        className="input-general login_input"
                        value={this.state.password}
                        onChange={(e) => this.onInputChange(e)}/>
                    <button type="submit" className="btn-general btn-confirm-visible login_btn-confirm">Change password</button>
                </form>
            </div>
        );
    }

    render(){
        return(
            <div className="cont-zone">
                <div className="big-header">
                    <img className="big-header_logo" src={Logo} alt="Arounda Agency"/>
                </div>
                {this.showContent()}
            </div>
        );
    }
}

PasswordReset.propTypes = {
    resetCode: PropTypes.string.isRequired,
    verifyResetCode: PropTypes.func.isRequired,
    confirmPasswordReset: PropTypes.func.isRequired
}

export default PasswordReset;