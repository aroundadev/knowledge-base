import React, {Component} from "react";
import PropTypes from "prop-types";

import Modal from "../Modal/Modal";

class AddEditGroupModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: this.props.title || ""
        };
    }

    onInputChange(e) {
        const name = e.target.name;

        let newState = {};
        newState[name] = e.target.value;

        this.setState(newState);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.groupToChange
            ? this.props.editGroup("userGroups", this.state.title, this.props.groupToChange)
            : this.props.addGroup("userGroups", this.state.title);
    }

    render() {
        return (
            <Modal
                toggleModal={() => this.props.toggleModal("addEditGroupModal")}
            >
                <div className="modal add-card">
                    <h3 className="modal_title">
                        {this.props.groupToChange
                            ? `Edit ${this.props.title} group `
                            : "Add new group"}
                    </h3>
                    <form
                        className="add-card_form"
                        id="addCardForm"
                        onSubmit={e => this.handleSubmit(e)}
                    >
                        <input
                            placeholder="Card's Title"
                            name="title"
                            type="text"
                            autoFocus
                            minLength="1"
                            className="input-general add-card_input"
                            value={this.state.title}
                            onChange={e => this.onInputChange(e)}
                        />
                        <button
                            type="button"
                            className="btn-general btn-cancel modal_btn"
                            onClick={() => this.props.toggleModal("addEditGroupModal")}
                        >
                            <p>Cancel</p>
                        </button>
                        <button type="submit" className="btn-general btn-confirm modal_btn">
                            <p>Save</p>
                        </button>
                    </form>
                </div>
            </Modal>
        );
    }
}

AddEditGroupModal.propTypes = {
    title: PropTypes.string,
    groupToChange: PropTypes.string,
    editGroup: PropTypes.func.isRequired,
    addGroup: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired
}

export default AddEditGroupModal;
