import React, {Component, Fragment} from "react";
import {isLoaded} from "react-redux-firebase";
import PropTypes from "prop-types";

import UserGroupsRow from "./UserGroupsRow";
import Card from "./Card";
import {dataObjectToArray} from "../../tools/toolFunctions";
import AddEditGroupModal from "./AddEditGroupModal";
import DeleteGroupModal from "./DeleteGroupModal";

import CrossAddWhite from "../../../assets/icons/cross-add-white.svg";

class AccessControl extends Component {
    constructor(props) {
        super(props);
        this.changeGlobalPermission = this.changeGlobalPermission.bind(this);
        this.changeLocalPermission = this.changeLocalPermission.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.editGroup = this.editGroup.bind(this);
        this.deleteGroup = this.deleteGroup.bind(this);
        this.toggleModal = this.toggleModal.bind(this);

        this.state = {
            groupToChange: undefined,
            deleteGroupModalOpen: false,
            addEditGroupModalOpen: false
        };
    }

    changeGlobalPermission(id, permission, value) {
        let newPermissions = this.props.userGroups[id].permissions || {};
        newPermissions[permission] = value;

        this.props.firestore.update(`userGroups/${id}`, {
            permissions: newPermissions
        });
    }

    changeLocalPermission(userGroupId, cardId, permission, value) {
        let newCantArray = this.props.userGroups[userGroupId][`cant-${permission}`] || [];
        let newGlobalPermissions = this.props.userGroups[userGroupId].permissions || {};

        if (value === true) {
            if (newCantArray.indexOf(cardId) !== -1) {
                newCantArray = newCantArray.filter(id => id !== cardId);
            }
            if (!newGlobalPermissions[permission]) {
                newGlobalPermissions[permission] = true;
            }

            this.props.firestore.update(`userGroups/${userGroupId}`, {
                permissions: newGlobalPermissions,
                [`cant-${permission}`]: newCantArray
            });
        } else {
            if (newCantArray.indexOf(cardId) === -1) {

                newCantArray.push(cardId);
            }

            this.props.firestore.update(`userGroups/${userGroupId}`, {
                [`cant-${permission}`]: newCantArray
            });
        }
    }

    toggleModal(modal, groupToChange) {
        if (groupToChange)
            this.setState({
                [`${modal}Open`]: !this.state[`${modal}Open`],
                groupToChange
            });
        else {
            this.setState({
                [`${modal}Open`]: !this.state[`${modal}Open`],
                groupToChange: undefined
            });
        }
    }

    addGroup(groups, title) {
        this.props.firestore
            .add(`${groups}/`, {title})
            .then(() => this.toggleModal("addEditGroupModal"));
    }

    editGroup(groups, title, id) {
        this.props.firestore
            .update(`${groups}/${id}`, {title})
            .then(() => this.toggleModal("addEditGroupModal"));
    }

    deleteGroup(groups, id) {
        this.props.firestore
            .delete(`${groups}/${id}`)
            .then(() => this.toggleModal("deleteGroupModal"));
    }

    render() {
        if (!isLoaded(this.props.userGroups, this.props.cards)) {
            return (
                <section className="settings_section">
                    <div className="settings_section_head">
                        <h1 className="settings_section_head_title">AccessControl</h1>
                    </div>
                    Loading
                </section>
            );
        }

        let userGroups = dataObjectToArray(this.props.userGroups),
            cards = dataObjectToArray(this.props.cards);


        

        return (
            <Fragment>
                <section className="settings_section">
                    <div className="settings_section_head">
                        <h1 className="settings_section_head_title">User Groups</h1>
                        <button
                            className="settings_section_head_link u-mng_add-link"
                            onClick={() => this.toggleModal("addEditGroupModal")}
                        >
                            <img src={CrossAddWhite} alt="" width="10" height="10" />
                            Add
                        </button>
                    </div>
                    <table className="settings_section_table">
                        <tbody>
                            {userGroups.map(group => {
                                let permissions = group.permissions || {};

                                return (
                                    <UserGroupsRow
                                        key={group.id}
                                        title={group.title}
                                        id={group.id}
                                        read={permissions.read}
                                        edit={permissions.edit}
                                        admin={permissions.admin}
                                        changePermission={this.changeGlobalPermission}
                                        handleDelete={() =>
                                            this.toggleModal("deleteGroupModal", group.id)
                                        }
                                        handleEdit={() =>
                                            this.toggleModal("addEditGroupModal", group.id)
                                        }
                                    />
                                );
                            })}
                        </tbody>
                    </table>
                </section>
                <section className="settings_section">
                    <div className="settings_section_head">
                        <h1 className="settings_section_head_title">Permissions by cards</h1>
                    </div>
                    <div className="access-control_cards_wrap">
                        {cards.map(card => (
                            <Card
                                key={card.id}
                                card={card}
                                userGroups={userGroups}
                                changeLocalPermission={this.changeLocalPermission}
                            />
                        ))}
                    </div>
                </section>
                {this.state.addEditGroupModalOpen && (
                    <AddEditGroupModal
                        addGroup={this.addGroup}
                        editGroup={this.editGroup}
                        toggleModal={this.toggleModal}
                        groupToChange={this.state.groupToChange}
                        title={this.props.userGroups[this.state.groupToChange] && this.props.userGroups[this.state.groupToChange].title}
                    />
                )}
                {this.state.deleteGroupModalOpen && (
                    <DeleteGroupModal
                        groupId={this.state.groupToChange}
                        title={this.props.userGroups[this.state.groupToChange]  && this.props.userGroups[this.state.groupToChange].title}
                        toggleModal={this.toggleModal}
                        deleteGroup={() => this.deleteGroup("userGroups", this.state.groupToChange)}
                    />
                )}
            </Fragment>
        );
    }
}

AccessControl.propTypes = {
    firestore: PropTypes.object.isRequired,
    userGroups: PropTypes.object,
    cards: PropTypes.object
}

export default AccessControl;
