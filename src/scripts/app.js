import React, {Component} from "react";
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import {withFirestore, isEmpty, isLoaded} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";
import Cookies from "js-cookie";
import Async from "react-code-splitting";
import PropTypes from "prop-types";

import CardsFeedContainer from "./components/Cards/CardsFeedContainer";
import Login from "./components/Auth/Login";
import LoginActions from "./components/Auth/AuthActions";

const Article = props => (
    <Async load={import("./components/Article/ArticleContainer")} componentProps={props} />
);
const Settings = props => (
    <Async load={import("./components/Settings/Settings")} componentProps={props} />
);
const Restore = props => (
    <Async load={import("./components/Auth/Restore")} componentProps={props} />
);

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            groupFetched: false
        };
    }

    componentDidUpdate() {
        if (this.props.profile.group_id && !this.state.groupFetched) {
            this.props.firestore
                .get({
                    collection: "userGroups",
                    doc: this.props.profile.group_id,
                    storeAs: "currentUserGroup"
                })
                .then(doc => {
                    if (doc.exists) {
                        this.setState({groupFetched: true});
                    } else {
                        this.props.firestore
                            .get({
                                collection: "userGroups",
                                doc: "G6AEPJADW1reAtb3YMb1",
                                storeAs: "currentUserGroup"
                            })
                            .then(() => this.setState({groupFetched: true}));
                    }
                });
        }
    }

    checkCookieAndLogin() {
        let user = Cookies.getJSON("user");

        if (user) {
            this.props.firebase.login({
                email: user.email,
                password: user.password
            });
        }
    }

    render() {
        let profile = this.props.profile;

        if (!isLoaded(profile)) {
            this.checkCookieAndLogin();
            return <p>Loading</p>;
        }

        if (isEmpty(profile)) {
            return (
                <Router>
                    <Switch>
                        <Route path="/login" component={Login} />
                        <Route path="/restore" component={Restore} />
                        <Route path="/auth/actions" render={() => <LoginActions />} />
                        <Route path="/" render={() => <Redirect to="/login" />} />
                    </Switch>
                </Router>
            );
        }

        return (
            <Router>
                <Switch>
                    <Route
                        exact
                        path="/"
                        render={() => <CardsFeedContainer profile={this.props.profile} />}
                    />
                    <Route path="/auth/actions/" render={() => <LoginActions />} />
                    <Route path="/article/:id" component={Article} />
                    <Route path="/settings" render={props => <Settings {...props} />} />
                    <Route path="/login" component={() => <Redirect to="/" />} />
                </Switch>
            </Router>
        );
    }
}

const mapStateToProps = state => ({
    profile: state.firebase.profile
});

App.propTypes = {
    profile: PropTypes.object.isRequired,
    firestore: PropTypes.object.isRequired,
    firebase: PropTypes.object.isRequired    
}

export default compose(withFirestore, connect(mapStateToProps))(App);
