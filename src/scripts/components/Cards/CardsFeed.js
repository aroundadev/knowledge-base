import React, {Component} from "react";
import noScroll from "no-scroll";
import shortid from "shortid";
import {isLoaded} from "react-redux-firebase";
import PropTypes from "prop-types";
import axios from "axios";

import HeaderMenu from "../HeaderMenu";
import Card from "./Card";
import AddEditCardModal from "./AddEditCardModal";
import DeleteCardModal from "./DeleteCardModal";

const Logo = require("../../../assets/icons/logotype.svg");
const CrossAdd = require("../../../assets/icons/cross-add.svg");

class CardsFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            addEditModalOpen: false,
            deleteModalOpen: false,
            cardToChange: undefined,
            newArticleContentPlaceholder:
        "<h1>Свежесозданная карточка.</h1><p>Хочется ее наполнить, да?</p>",
            status: "idle"
        };
    }

    toggleAddEditModal(id) {
        noScroll.toggle();

        if (!id) {
            this.setState({
                addEditModalOpen: !this.state.addEditModalOpen,
                cardToChange: undefined
            });
        } else {
            this.setState({
                addEditModalOpen: !this.state.addEditModalOpen,
                cardToChange: this.props.cards.filter(card => card.id === id)[0]
            });
        }
    }

    toggleDeleteModal(id = "") {
        noScroll.toggle();

        if (!this.state.deleteModalOpen) {
            this.setState({
                deleteModalOpen: true,
                cardToChange: this.props.cards.filter(card => card.id === id)[0]
            });
        } else {
            this.setState({
                deleteModalOpen: false,
                cardToChange: undefined,
                status: "idle"
            });
        }
    }

    addCard(title, group_id) {
        this.toggleAddEditModal();

        const id = shortid.generate();
        const date = Date.now();
        this.props.firestore.set(`cards/${id}`, {title, group_id, date});
        this.props.firestore.set(`cardArticles/${id}`, {
            card_id: id,
            card_title: title,
            content: this.state.newArticleContentPlaceholder
        });
    }

    editCard(title, group_id, id) {
        this.toggleAddEditModal();

        this.props.firestore.update(`cards/${id}`, {title, group_id});
        this.props.firestore.update(`cardArticles/${id}`, {card_title: title});
    }

    deleteCard() {
        const apiURL = `https://us-central1-arounda-kb.cloudfunctions.net/api/card?id=${
            this.state.cardToChange.id
        }`;

        // const apiURL = `http://localhost:5000/arounda-kb/us-central1//api/card?id=${this.state.cardToChange.id}`;

        this.setState(
            {
                status: "deleting"
            },
            () => {
                this.props.firebase
                    .auth()
                    .currentUser.getIdToken()
                    .then(token => {
                        axios
                            .delete(apiURL, {
                                headers: {"X-User-Token": token}
                            })
                            .then(() => {
                                this.setState({
                                    status: "success"
                                });
                            })
                            .catch(() => {
                                this.setState({
                                    status: "error"
                                });
                            });
                    });
            }
        );
    }

    ifReadable(userGroup, id) {
        if (userGroup.permissions && !userGroup.permissions.read) {
            return false;
        }
        if (userGroup["cant-read"] && userGroup["cant-read"].indexOf(id) !== -1) {
            return false;
        }
        return true;
    }

    render() {
        const {cards, cardGroups, currentUserGroup} = this.props;

        if (!isLoaded(cards, cardGroups, currentUserGroup)) {
            return <div>Loading</div>;
        }

        return (
            <div className="cont-zone">
                <div className="cards-feed_wrap">
                    <div className="big-header">
                        <img className="big-header_logo" src={Logo} alt="Arounda Agency" />
                        <HeaderMenu />
                    </div>
                    <div className="cards-feed">
                        {cards.map((card, index) => {
                            const cardGroup = cardGroups.filter(group => group.id === card.group_id)[0];

                            const readable = this.ifReadable(currentUserGroup, card.id);

                            return (
                                <Card
                                    key={index}
                                    title={card.title}
                                    id={card.id}
                                    group={cardGroup.title}
                                    toggleDeleteModal={() => this.toggleDeleteModal(card.id)}
                                    toggleEditModal={() => this.toggleAddEditModal(card.id)}
                                    admin={currentUserGroup.permissions.admin}
                                    readable={readable}
                                />
                            );
                        })}
                        {currentUserGroup.permissions.admin && (
                            <div className="card-adder" onClick={() => this.toggleAddEditModal()}>
                                <img src={CrossAdd} alt="Add" />
                                <p>Add a new card</p>
                            </div>
                        )}
                    </div>
                    {this.state.addEditModalOpen && (
                        <AddEditCardModal
                            cardGroups={cardGroups}
                            toggleModal={() => this.toggleAddEditModal()}
                            card={this.state.cardToChange}
                            addCard={(title, group_id) => this.addCard(title, group_id)}
                            editCard={(title, group_id, id) => this.editCard(title, group_id, id)}
                        />
                    )}
                    {this.state.deleteModalOpen && (
                        <DeleteCardModal
                            card={this.state.cardToChange}
                            toggleModal={() => this.toggleDeleteModal()}
                            deleteCard={() => this.deleteCard()}
                            status={this.state.status}
                        />
                    )}
                </div>
            </div>
        );
    }
}

CardsFeed.propTypes = {
    cardGroups: PropTypes.arrayOf(PropTypes.object),
    cards: PropTypes.arrayOf(PropTypes.object),
    currentUserGroup: PropTypes.object,
    firestore: PropTypes.object.isRequired,
    firebase: PropTypes.object.isRequired,
    profile: PropTypes.object.isRequired
};

export default CardsFeed;
