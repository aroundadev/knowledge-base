import React, {Component} from "react";
import PropTypes from "prop-types";

import Modal from "../Modal/Modal";

class AddEditCardModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            addCardTitle: this.props.card ? this.props.card.title : "",
            addCardGroupId: this.props.card ? this.props.card.group_id : this.props.cardGroups[0].id
        };
    }

    onInputChange(e) {
        const name = e.target.name;

        let newState = {};
        newState[name] = e.target.value;

        this.setState(newState);
    }

    handleSubmit(e) {
        e.preventDefault();

        if (this.props.card) {
            return this.props.editCard(
                this.state.addCardTitle,
                this.state.addCardGroupId,
                this.props.card.id
            );
        }

        return this.props.addCard(this.state.addCardTitle, this.state.addCardGroupId);
    }

    render() {
        const cardToChange = this.props.card;
        return (
            <Modal toggleModal={() => this.props.toggleModal()}>
                <div className="modal add-card">
                    <h3 className="modal_title">
                        {cardToChange ? `Edit card ${cardToChange.title}` : "Add new card"}
                    </h3>
                    <form
                        className="add-card_form"
                        id="addCardForm"
                        onSubmit={e => this.handleSubmit(e)}
                    >
                        <input
                            placeholder="Card's Title"
                            name="addCardTitle"
                            type="text"
                            autoFocus
                            minLength="1"
                            className="input-general add-card_input"
                            value={this.state.addCardTitle}
                            onChange={e => this.onInputChange(e)}
                        />
                        <select
                            value={this.state.addCardGroupId}
                            name="addCardGroupId"
                            id=""
                            className="input-general add-card_input"
                            onChange={e => this.onInputChange(e)}
                        >
                            {this.props.cardGroups.map((group, index) => (
                                <option key={index} value={group.id}>
                                    {group.title}
                                </option>
                            ))}
                        </select>
                        <button
                            type="button"
                            className="btn-general btn-cancel modal_btn"
                            onClick={() => this.props.toggleModal()}
                        >
                            <p>Cancel</p>
                        </button>
                        <button type="submit" className="btn-general btn-confirm modal_btn">
                            <p>Save</p>
                        </button>
                    </form>
                </div>
            </Modal>
        );
    }
}

AddEditCardModal.propTypes = {
    card: PropTypes.object,
    cardGroups: PropTypes.arrayOf(PropTypes.object).isRequired,
    editCard: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    addCard: PropTypes.func.isRequired
}

export default AddEditCardModal;
