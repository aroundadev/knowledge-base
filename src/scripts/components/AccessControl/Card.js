import React, {Component} from "react";
import PropTypes from "prop-types";

class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    render() {
        let open = this.state.open;

        let card = this.props.card;

        const checkPermission = (userGroup, permission) => {
            let cantArray = userGroup[`cant-${permission}`] || [];
            let permissions = userGroup.permissions || {};

            if(permissions[permission] === true && cantArray.indexOf(card.id) === -1) {
                return true;
            }
            else {
                return false;
            }
        }

        return (
            <div className={`access-control_card ${open && "-open"}`}>
                <div
                    className="access-control_card_head"
                    onClick={() => this.setState({open: !open})}
                >
                    <h4 className="access-control_card_head_title">{card.title}</h4>
                </div>
                {open && (
                    <ul className="access-control_card_list">
                        {this.props.userGroups.map(userGroup => {
                            let canRead = checkPermission(userGroup, "read");
                            let canEdit = checkPermission(userGroup, "edit");                            

                            return (
                                <li key={userGroup.id} className="access-control_card_list_item">
                                    {userGroup.title}
                                    <ul className="access-control_card_sublist">
                                        <li>
                                            <div className="checkbox-general access-control_card_checkbox">
                                                <input
                                                    type="checkbox"
                                                    id={`${userGroup.id}-read-${card.id}`}
                                                    name="canRead"
                                                    checked={canRead}
                                                    value={true}
                                                    onChange={() => this.props.changeLocalPermission(userGroup.id, card.id, "read", !canRead)}
                                                />
                                                <label htmlFor={`${userGroup.id}-read-${card.id}`}>
                                                    <span>reading</span>
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox-general access-control_card_checkbox">
                                                <input
                                                    type="checkbox"
                                                    id={`${userGroup.id}-edit-${card.id}`}
                                                    name="canEdit"
                                                    checked={canEdit}
                                                    value={true}
                                                    onChange={() => this.props.changeLocalPermission(userGroup.id, card.id, "edit", !canEdit)}
                                                />
                                                <label htmlFor={`${userGroup.id}-edit-${card.id}`}>
                                                    <span>editing</span>
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            );
                        })}
                    </ul>
                )}
            </div>
        );
    }
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
    userGroups: PropTypes.arrayOf(PropTypes.object).isRequired,
    changeLocalPermission: PropTypes.func.isRequired
}

export default Card;
