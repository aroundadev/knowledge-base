const admin = require("firebase-admin");

const deleteCard = (req, res) => {
    const id = req.query.id;

    const deleteArticleDoc = () => {
        admin
            .firestore()
            .collection("cardArticles")
            .doc(id)
            .delete()
            .then(() => res.status(200).send("Card was deleted successfully"))
            .catch(() => res.status(404).send("Card was not found"));
    };

    admin
        .firestore()
        .collection("cards")
        .doc(id)
        .delete()
        .then(() => deleteArticleDoc())
        .catch(() => res.status(404).send("Card was not found"));
};

module.exports = {
    deleteCard
};
