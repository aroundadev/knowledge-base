import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";

import {orderedArrayToDataObject} from "../../tools/toolFunctions";

import AccessControl from "./AccessControl";

// const mapStateToProps = state => ({
//     userGroups: state.firestore.data.userGroups ? state.firestore.data.userGroups : {},
//     cards: state.firestore.data.cards ? state.firestore.data.cards : {}
// });

const mapStateToProps = state => ({
    userGroups: state.firestore.ordered.userGroups ? orderedArrayToDataObject(state.firestore.ordered.userGroups) : {},
    cards: state.firestore.ordered.cards ? orderedArrayToDataObject(state.firestore.ordered.cards) : {}
});

const AccessControlContainer = compose(
    firestoreConnect(() => [
        {collection: "userGroups", storeAs: "userGroups"},
        {collection: "cards", storeAs: "cards"}
    ]),
    connect(mapStateToProps)
)(AccessControl);

export default AccessControlContainer;