const ContentParser = {
    getSectionStrings(content, headingLevel) {
        const regexp = new RegExp(`(?!<${headingLevel}><br></${headingLevel}>)(<${headingLevel}>(.+?)</${headingLevel}>)`, "g");
        return content.match(regexp);
    },

    getTitlesAndInsertIDs(sectionsStrings, headingLevel) {
        let sectionTitles = [];
        const regexp = new RegExp(`(</?${headingLevel}>)`, "g");

        let sectionsWithIDs = sectionsStrings.map((section, index) => {
            const sectionTitle = section.replace(regexp, "");
            sectionTitles.push(sectionTitle);

            return section.replace(`<${headingLevel}>`, `<${headingLevel} id="${index}_${sectionTitle}">`);
        });

        return {sectionTitles, sectionsWithIDs};
    },

    putIDsIntoContent(content, sections, sectionsWithIDs) {
        let updatedContent = content;

        sectionsWithIDs.forEach((item, index) => {
            updatedContent = updatedContent.replace(sections[index], item);
        });

        return updatedContent;
    },

    generateNewStructure(sectionTitles, subsectionTitles) {
        let structure = {};
        
        sectionTitles.forEach((title, index) => structure[`${index}__${title}`] = subsectionTitles[index] || []);

        return structure;
    },

    getSubsections(sectionsWithIDs, content) {
        let subsectionsStringsArray = [];
        let subsectionTitles = [];
        let updatedContentWithSubsections = content;

        //get subsections arrays for each section, null if none
        sectionsWithIDs.forEach((title, index, array) => {
            let wholeSection = "";
            const startIndex = content.lastIndexOf(title);
            if (index + 1 < array.length) {
                const finishIndex = content.indexOf(array[index + 1]);

                wholeSection = content.substring(startIndex, finishIndex);
            }
            else {
                wholeSection = content.substring(startIndex);
            }
            const subsectionsStrings = this.getSectionStrings(wholeSection, "h2");

            subsectionsStringsArray.push(subsectionsStrings);
        });

        //parse each subsections array
        subsectionsStringsArray.forEach(array => {
            if (!array) {
                subsectionTitles.push(null);
                return;
            }
            const {sectionTitles, sectionsWithIDs} = this.getTitlesAndInsertIDs(array, "h2");
            updatedContentWithSubsections = this.putIDsIntoContent(updatedContentWithSubsections, array, sectionsWithIDs);
            subsectionTitles.push(sectionTitles);
        });

        return {subsectionTitles, updatedContentWithSubsections};
    },

    parseNewContent(content) {
        const sectionsStrings = this.getSectionStrings(content, "h1");
        if (!sectionsStrings) {
            return {structure: {}, content};
        }

        const {sectionTitles, sectionsWithIDs} = this.getTitlesAndInsertIDs(sectionsStrings, "h1");
        const updatedContent = this.putIDsIntoContent(content, sectionsStrings, sectionsWithIDs);
        const {subsectionTitles, updatedContentWithSubsections} = this.getSubsections(sectionsWithIDs, updatedContent);
        const newStructure = this.generateNewStructure(sectionTitles, subsectionTitles);

        return {structure: newStructure, content: updatedContentWithSubsections};
    }
};

export default ContentParser;