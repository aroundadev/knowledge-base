import React, {Component} from "react";
import PropTypes from "prop-types";

class InputGeneral extends Component {
    constructor(props) {
        super(props);
        this.blurHandler = this.blurHandler.bind(this);

        this.state = {
            touched: false
        }
    }

    blurHandler() {
        if(this.props.settings.onBlurFunc) {
            this.props.settings.onBlurFunc();
        }
        else if (!this.state.touched) {
            this.setState({touched: true});
        }
    }

    render() {
        const {settings, valueState, errorState, errorText, addClasses, changeHandler} = this.props;

        return (
            <div className="input-general_with-error_wrap">
                <p className={`input-general_error ${errorState ? "shown" : ""}`}>
                    {errorText}
                </p>
                <input
                    required={settings.required}
                    placeholder={settings.placeholder}
                    name={settings.name}
                    type={settings.type || "text"}
                    onBlur = {() => this.blurHandler()}
                    className={`input-general ${this.state.touched ? "-touched" : ""} ${addClasses} ${errorState ? "invalid" : ""}`}
                    value={valueState}
                    onChange={e => changeHandler(e)}
                />
            </div>
        );
    }
} 

InputGeneral.propTypes = {
    settings: PropTypes.object.isRequired,
    valueState: PropTypes.string.isRequired,
    errorState: PropTypes.bool.isRequired,
    errorText: PropTypes.string.isRequired,
    addClass: PropTypes.string,
    changeHandler: PropTypes.func.isRequired,
    addClasses: PropTypes.string
}

export default InputGeneral;
