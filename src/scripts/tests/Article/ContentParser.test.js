import ContentParser from "../../components/Article/ContentParser";

describe("ContentParser entry point - parseNewContent method", () => {
    test("empty content should return empty object", () => {
        expect(ContentParser.parseNewContent("")).toEqual({structure: {}, content: ""});
    });

    test("content without h1 tags should return text without structure", () => {
        const testContent = "Some content to test. <p>See, we have p-tags too</p>";
        expect(ContentParser.parseNewContent(testContent)).toEqual({structure: {}, content: testContent});
    });

    test("content without h1 tags should return text without structure even with h2 tags", () => {
        const testContent = "Some content to test. <h2>Secondary title</h2> <p>See, we have p-tags too</p>";
        expect(ContentParser.parseNewContent(testContent)).toEqual({structure: {}, content: testContent});
    });

    test("content with h1 tags should return updated content with new structure", () => {
        const testContent = "<h1>I'm a title</h1> And I'm not <h1>I'm also a title</h1>";

        const result = {
            structure: {
                ["0__I'm a title"]: [],
                ["1__I'm also a title"]: []
            },
            content:
            "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not " +
            "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"
        };
        expect(ContentParser.parseNewContent(testContent)).toEqual(result);
    });

    test("content with h1 and h2 tags should return updated content with new proper structure", () => {
        const testContent =
            "<h1>I'm a title</h1> And I'm not " +
            "<h2>Subsection title here</h2> Subsection text " +
            "<h1>I'm also a title</h1>";

        const result = {
            structure: {
                ["0__I'm a title"]: ["Subsection title here"],
                ["1__I'm also a title"]: []
            },
            content:
            "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not " +
            "<h2 id=\"0_Subsection title here\">Subsection title here</h2> Subsection text " +
            "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"
        };
        expect(ContentParser.parseNewContent(testContent)).toEqual(result);
    });
});

describe("getSectionStrings method", () => {
    test("should return null if no tags", () => {
        const testContent = "No tags here!";
        expect(ContentParser.getSectionStrings(testContent, "h1")).toEqual(null);
    });

    test("should find h1-tags", () => {
        const testContent = "<h1>I'm a title</h1> And I'm not <h1>I'm also a title</h1>";
        expect(ContentParser.getSectionStrings(testContent, "h1")).toEqual(["<h1>I'm a title</h1>", "<h1>I'm also a title</h1>"]);
    });
});

describe("getTitlesAndInsertIDs method", () => {
    test("should return title and insert ids", () => {
        const tagsArray = ["<h1>I'm a title</h1>", "<h1>I'm also a title</h1>"];
        const result = {
            sectionTitles: ["I'm a title", "I'm also a title"],
            sectionsWithIDs: ["<h1 id=\"0_I'm a title\">I'm a title</h1>", "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"]
        };
        expect(ContentParser.getTitlesAndInsertIDs(tagsArray, "h1")).toEqual(result);
    });

    test("if no tags should return empty results", () => {
        const tagsArray = [];
        const result = {
            sectionTitles: [],
            sectionsWithIDs: []
        };
        expect(ContentParser.getTitlesAndInsertIDs(tagsArray, "h1")).toEqual(result);
    });
});

describe("putIDsIntoContent method", () => {
    test("should replace content title tags with tags with id", () => {
        const testContent = "<h1>I'm a title</h1> And I'm not <h1>I'm also a title</h1>";
        const tagsArray = ["<h1>I'm a title</h1>", "<h1>I'm also a title</h1>"];
        const sectionsWithIDs = ["<h1 id=\"0_I'm a title\">I'm a title</h1>", "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"];

        const result = "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not <h1 id=\"1_I'm also a title\">I'm also a title</h1>";

        expect(ContentParser.putIDsIntoContent(testContent, tagsArray, sectionsWithIDs)).toEqual(result);
    });
});

describe("getSubsections method", () => {
    test("should find all h2 tags, give them ids and update content", () => {
        const testContent =
            "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not " +
            "<h2>Subsection title here</h2> Subsection text " +
            "<h1 id=\"1_I'm also a title\">I'm also a title</h1>";
        const sectionsWithIDs = ["<h1 id=\"0_I'm a title\">I'm a title</h1>", "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"];

        const result = {
            subsectionTitles: [["Subsection title here"], null],
            updatedContentWithSubsections:
                "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not " +
                "<h2 id=\"0_Subsection title here\">Subsection title here</h2> Subsection text " +
                "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"
        };

        expect(ContentParser.getSubsections(sectionsWithIDs, testContent)).toEqual(result);

    });

    test("if no subsections, should return array of nulls and doesn't change content", () => {
        const testContent =
            "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not " +
            "<h1 id=\"1_I'm also a title\">I'm also a title</h1>";
        const sectionsWithIDs = ["<h1 id=\"0_I'm a title\">I'm a title</h1>", "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"];

        const result = {
            subsectionTitles: [null, null],
            updatedContentWithSubsections:
            "<h1 id=\"0_I'm a title\">I'm a title</h1> And I'm not " +
            "<h1 id=\"1_I'm also a title\">I'm also a title</h1>"
        };

        expect(ContentParser.getSubsections(sectionsWithIDs, testContent)).toEqual(result);
    });

    describe("generateNewSctructure method", () => {
        test ("should return valid structure", () => {
            const sectionTitles = ["I'm a title", "I'm also a title"];
            const subsectionTitles = [["Subsection title here"], null];

            const result = {
                ["0__I'm a title"]: ["Subsection title here"],
                ["1__I'm also a title"]: []
            };

            expect(ContentParser.generateNewStructure(sectionTitles, subsectionTitles)).toEqual(result);            
        });

        test("if no subsections should return only section titles", () => {
            const sectionTitles = ["I'm a title", "I'm also a title"];
            const subsectionTitles = [null, null];

            const result = {
                ["0__I'm a title"]: [],
                ["1__I'm also a title"]: []
            };
        
            expect(ContentParser.generateNewStructure(sectionTitles, subsectionTitles)).toEqual(result);
        });
    });
});
