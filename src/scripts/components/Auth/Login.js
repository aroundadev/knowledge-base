import React, {Component} from "react";
import {firestoreConnect} from "react-redux-firebase";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

import InputGeneral from "../General/InputGeneral";

const Logo = require("../../../assets/icons/logotype.svg");

class Login extends Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            email: "",
            password: "",
            emailError: false,
            passwordError: false
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            emailError: false,
            passwordError: false
        });

        this.props.firebase
            .login({
                email: this.state.email,
                password: this.state.password
            })
            .catch(error => {
                switch (error.code) {
                case "auth/invalid-email": {
                    this.setState({
                        emailError: true
                    });
                    break;
                }
                case "auth/user-not-found": {
                    this.setState({
                        emailError: true
                    });
                    break;
                }
                case "auth/wrong-password": {
                    this.setState({
                        passwordError: true
                    });
                    break;
                }
                }
            });
    }

    onInputChange(e) {
        const name = e.target.name;

        let newState = {};
        newState[name] = e.target.value;

        this.setState(newState);
    }

    render() {
        return (
            <div className="cont-zone">
                <div className="big-header">
                    <img className="big-header_logo" src={Logo} alt="Arounda Agency" />
                </div>
                <div className="login_container">
                    <h1 className="login_heading">Log-in to your account</h1>
                    <form className="login_form" onSubmit={e => this.handleSubmit(e)}>
                        <InputGeneral
                            settings={{
                                placeholder: "Email",
                                type: "email",
                                name: "email",
                                required: true
                            }}
                            valueState={this.state.email}
                            errorState={this.state.emailError}
                            errorText="No user with such email"
                            changeHandler={this.onInputChange}
                            addClasses="settings_input"
                        />
                        <InputGeneral
                            settings={{
                                placeholder: "Password",
                                type: "password",
                                name: "password",
                                required: true
                            }}
                            valueState={this.state.password}
                            errorState={this.state.passwordError}
                            errorText="Oops, this password is incorrect"
                            changeHandler={this.onInputChange}
                            addClasses="settings_input"
                        />
                        <Link to="/restore" className="login_btn-restore">
                            Forgot password?
                        </Link>
                        <button
                            type="submit"
                            className="btn-general btn-confirm-visible login_btn-confirm"
                        >
                            Login
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

Login.contextTypes = {
    router: PropTypes.object.isRequired
};

Login.propTypes = {
    firebase: PropTypes.object.isRequired
}

export default firestoreConnect()(Login);
