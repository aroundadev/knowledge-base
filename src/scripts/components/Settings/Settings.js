import React, {Component, Fragment} from "react";
import {Link, Route, Switch, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import HeaderMenu from "../HeaderMenu";
import UserManagement from "../UserManagement/UserManagementContainer";
import CreateUser from "../CreateUser/CreateUserContainer";
import EditUser from "../EditUser/EditUserContainer";
import AccessControl from "../AccessControl/AccessControlContainer";

class Settings extends Component {
    render() {
        const {history, uid, currentUserGroup} = this.props;

        if (!currentUserGroup) {
            return <div>Loading</div>;
        }

        return (
            <div className="cont-zone cont-zone-page">
                <div className="page">
                    <div className="page_menu">
                        <div className="page_menu_content">
                            <h3 className="page_menu_title">Settings</h3>
                            <ol className="page_menu_list">
                                {currentUserGroup.permissions.admin && (
                                    <Fragment>
                                        <li className="page_menu_list_section">
                                            <Link to="/settings/user-management">
                                                User management
                                            </Link>
                                        </li>
                                        <li className="page_menu_list_section">
                                            <Link to="/settings/access-control">
                                                Access control
                                            </Link>
                                        </li>
                                    </Fragment>
                                )}
                                <li className="page_menu_list_section">
                                    <Link to={`/settings/edit-profile/${uid}`}>Edit profile</Link>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div className="page_content_wrap">
                        <div className="page_upper-row">
                            <button className="page_back-btn" onClick={() => history.push("/")}>
                                To All Cards
                            </button>
                            <HeaderMenu />
                        </div>
                        {currentUserGroup.permissions.admin ? (
                            <Fragment>
                                <Route
                                    path="/settings/user-management"
                                    component={UserManagement}
                                />
                                <Route path="/settings/create-user" component={CreateUser} />
                                <Route path="/settings/edit-profile/:id" component={EditUser} />
                                <Route path="/settings/access-control" component={AccessControl} />
                            </Fragment>
                        ) : (
                            <Switch>
                                <Route
                                    exact
                                    path={"/settings/edit-profile/:id"}
                                    render={props => props.match.params.id === uid ? <EditUser {...props} /> : <Redirect to="/" />}
                                />
                                <Route path="/" render={() => <Redirect to="/" />} />
                            </Switch>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    uid: state.firebase.auth.uid,
    currentUserGroup: state.firestore.data.currentUserGroup
});

Settings.propTypes = {
    uid: PropTypes.string.isRequired,
    currentUserGroup: PropTypes.object,
    history: PropTypes.object.isRequired
}

export default connect(mapStateToProps)(Settings);

// export default Settings;
