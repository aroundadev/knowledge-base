import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";

import UserManagement from "./UserManagement.js";
import {orderedArrayToDataObject} from "../../tools/toolFunctions";

const mapStateToProps = state => ({
    users: state.firestore.ordered.users && orderedArrayToDataObject(state.firestore.ordered.users),
    userGroups: state.firestore.ordered.userGroups && orderedArrayToDataObject(state.firestore.ordered.userGroups)
});

const UserManagementContainer = compose(
    firestoreConnect(() => [
        {collection: "users", storeAs: "users"},
        {collection: "userGroups", storeAs: "userGroups"}
    ]),
    connect(mapStateToProps)
)(UserManagement);

export default UserManagementContainer;
