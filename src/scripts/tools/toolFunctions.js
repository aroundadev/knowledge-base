export function dataObjectToArray(obj) {
    let array = [];

    Object.keys(obj).forEach(id => {
        let item = obj[id];
        item.id = id;
        array.push(item);
    });

    return array;
}

export function orderedArrayToDataObject(orderedArray) {
    let newObj = {};

    orderedArray.forEach(item => {
        newObj[item.id] = item;
    })

    return newObj
}