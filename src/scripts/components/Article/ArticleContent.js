import React, {Fragment} from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const editIcon = require("../../../assets/icons/edit-icon-white.svg");

const ArticleContent = ({content, id, editable}) => (
    <Fragment>
        <div className="article_content ql-editor" dangerouslySetInnerHTML={{__html: content}} />
        {editable && (
            <Link to={`/article/${id}/edit`} className="article_edit-btn">
                <img src={editIcon} alt="Редактировать" />
            </Link>
        )}
    </Fragment>
);

ArticleContent.propTypes = {
    content: PropTypes.string,
    id: PropTypes.string,
    editable: PropTypes.bool,
}

export default ArticleContent;
