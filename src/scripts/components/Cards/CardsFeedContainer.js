import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";
import PropTypes from "prop-types";

import CardsFeed from "./CardsFeed";

const sortDataBy = (data = [], sortBy = "date") => {
    return data.sort((a, b) => {
        return b[sortBy] - a[sortBy];
    });
};

const mapStateToProps = state => {
    
    return ({
        cards: state.firestore.ordered.cards && sortDataBy(state.firestore.ordered.cards),    
        cardGroups: state.firestore.ordered.cardGroups && state.firestore.ordered.cardGroups,
        currentUserGroup: state.firestore.data.currentUserGroup && state.firestore.data.currentUserGroup
    });
} 

const CardsFeedContainer = compose(
    firestoreConnect(props => ([{collection:"cardGroups"}, {collection:"cards"}, {
        collection: "userGroups",
        doc: props.profile.group_id,
        storeAs: "currentUserGroup"
    }])),
    connect(mapStateToProps))(CardsFeed);

CardsFeedContainer.propTypes = {
    profile: PropTypes.object
}

export default CardsFeedContainer;
