import "./styles.scss";

import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";

import store from "./scripts/firebaseStore";

import App from "./scripts/app";

class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}

render(<Root/>, document.getElementById("root"));