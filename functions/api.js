const express = require("express");
const cors = require("cors");
const admin = require("firebase-admin");

const appCredentials = require("./appCredentials");

const {createUser, editUser, deleteUser} = require("./api-functions/userFunctions.js");
const {deleteCard} = require("./api-functions/cardFunctions");
const {checkIfAdmin} = require("./api-middlware/permChecks");

const api = express();

admin.initializeApp(
    {
        credential: admin.credential.cert(appCredentials),
        databaseURL: "https://arounda-kb.firebaseio.com"
    },
    "AroundaKBApi"
);

const whitelist = ["http://localhost:9000", "https://arounda-kb.firebaseapp.com"];
const corsOptions = {
    origin: function(origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error("Not allowed by CORS"));
        }
    }
};

api.use(cors(corsOptions));

api.post("/user", checkIfAdmin, createUser);
api.patch("/user", checkIfAdmin, editUser);
api.delete("/user", checkIfAdmin, deleteUser);

api.delete("/card", checkIfAdmin, deleteCard);

module.exports = api;

