import React, {Component} from "react";
import ReactQuill from "react-quill";
import PropTypes from "prop-types";

class ArticleEditor extends Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.saveChanges = this.saveChanges.bind(this);

        this.state = {
            content: this.props.content,
        };
    }

    saveChanges() {
        this.props.handleArticleUpdate(this.state.content);
    }

    handleChange(value) {
        this.setState(
            {content: value}
        );
    }

    render() {
        return (
            <div className="article_editor_wrap">
                <ReactQuill theme="snow"
                    value={this.state.content}
                    onChange={this.handleChange}
                    modules={ArticleEditor.modules}/>
                <button className="btn-general btn-confirm-visible article_editor_btn" onClick={this.saveChanges}>Save</button>
                <button className="btn-general btn-cancel-visible article_editor_btn" onClick={this.context.router.history.goBack}>Back</button>
            </div>
        );
    }
}

ArticleEditor.modules = {
    toolbar: [
        [{"header": "1"}, {"header": "2"}],
        [{size: []}],
        ["bold", "italic", "underline", "strike", "blockquote"],
        [{"list": "ordered"}, {"list": "bullet"},
            {"indent": "-1"}, {"indent": "+1"}, { "align": [] }],
        ["link", "image"]
    ]
};

ArticleEditor.contextTypes = {
    router: PropTypes.object.isRequired
};

ArticleEditor.propTypes = {
    handleArticleUpdate: PropTypes.func.isRequired,
    content: PropTypes.string
}

export default ArticleEditor;