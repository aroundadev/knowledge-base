import React, {Fragment} from "react";
import PropTypes from "prop-types";

import Modal from "../Modal/Modal";

const DeleteCardModal = ({card, toggleModal, deleteCard, status}) => {
    let statusText = `Are you sure that you that you want to delete ${card.title}?`;
    switch (status) {
    case "idle": {
        statusText = `Are you sure that you that you want to delete ${card.title}?`;
        break;
    }
    case "deleting": {
        statusText = "The card is being deleted...";
        break;
    }
    case "success": {
        statusText = "The card is deleted!";
        break;
    }
    case "error": {
        statusText = "Something went wrong in the process!";
        break;
    }
    }

    return (
        <Modal toggleModal={() => toggleModal()}>
            <div className="modal">
                <h3 className="modal_title">{statusText}</h3>
                {status === "idle" && (
                    <Fragment>
                        <button
                            type="button"
                            className="btn-general btn-cancel modal_btn"
                            onClick={() => toggleModal()}
                        >
                            <p>No</p>
                        </button>
                        <button
                            type="button"
                            className="btn-general btn-confirm modal_btn"
                            onClick={() => deleteCard(card.id)}
                        >
                            <p>Yes</p>
                        </button>
                    </Fragment>
                )}

                {(status === "success" || status === "error") && (
                    <button
                        type="button"
                        className="btn-general btn-confirm-visible modal_btn"
                        onClick={() => toggleModal()}
                    >
                        <p>Ok</p>
                    </button>
                )}
            </div>
        </Modal>
    );
};

DeleteCardModal.propTypes = {
    card: PropTypes.object.isRequired,
    toggleModal: PropTypes.func.isRequired,
    deleteCard: PropTypes.func.isRequired,
    status: PropTypes.string.isRequired
};

export default DeleteCardModal;
