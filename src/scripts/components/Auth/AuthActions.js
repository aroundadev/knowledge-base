import React, {Component} from "react";
import PropTypes from "prop-types";
import {Redirect} from "react-router-dom";
import {firestoreConnect} from "react-redux-firebase";

import PasswordReset from "./PasswordReset";

class LoginActions extends Component {
    constructor(props) {
        super(props);


        this.state = {
            mode: undefined,
            oobCode: undefined,
        };
    }

    componentWillMount() {
        if (this.context.router.route.location.search) {
            const urlVariablesNames = ["mode", "oobCode"];
            const urlVariablesStringsArray = this.context.router.route.location.search.split("&");

            urlVariablesNames.forEach((name, index) => {
                const variable = urlVariablesStringsArray[index].substring(urlVariablesStringsArray[index].indexOf(name) + name.length + 1);
                this.setState({
                    [name]: variable
                });
            });
        }
    }

    render() {

        if (this.state.mode === "resetPassword") {
            return (
                <PasswordReset resetCode={this.state.oobCode}
                    verifyResetCode={this.props.firebase.verifyPasswordResetCode}
                    confirmPasswordReset={this.props.firebase.confirmPasswordReset}/>
            );
        }

        return (
            <Redirect to="/"/>
        );
    }
}

LoginActions.contextTypes = {
    router: PropTypes.object.isRequired
};

LoginActions.propTypes = {
    firebase: PropTypes.object.isRequired
}

export default firestoreConnect()(LoginActions);