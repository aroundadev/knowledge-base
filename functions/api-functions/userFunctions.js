const functions = require("firebase-functions");
const nodemailer = require("nodemailer");
const admin = require("firebase-admin");

const gmailEmail = encodeURIComponent(functions.config().gmail.email);
const gmailPassword = encodeURIComponent(functions.config().gmail.password);

const mailTransport = nodemailer.createTransport(
    `smtps://${gmailEmail}:${gmailPassword}@smtp.gmail.com`
);

const createUser = (req, res) => {
    const data = req.body;
    const sendEmail = req.query.email;

    admin
        .auth()
        .getUserByEmail(data.email)
        .then(() => res.status(400).send("User with this email already exists"))
        .catch(() => createUser(data, sendEmail));

    const createUser = data => {
        admin
            .auth()
            .createUser({
                displayName: data.name,
                email: data.email,
                password: data.password
            })
            .then(record => {
                createFirestoreDoc(record, data.groupId);
            })
            .catch(error => res.status(400).send(error));
    };

    const createFirestoreDoc = (userRecord, group_id) => {
        admin
            .firestore()
            .collection("users")
            .doc(userRecord.uid)
            .set({
                name: userRecord.displayName,
                email: userRecord.email,
                group_id
            })
            .then(() => {
                if (sendEmail) {
                    sendEmailToUser(userRecord);
                } else {
                    res.status(200).send(`User ${userRecord.displayName} was created`);
                }
            })
            .catch(error => {
                res.status(400).send(error);
            });
    };

    const sendEmailToUser = record => {
        const mailOptions = {
            from: gmailEmail,
            to: record.email,
            subject: "Welcome to Arounda Knowledge Base",
            text: `${
                record.displayName
            }, your Arounda Knowledge Base user account was just created. 
            Now you can proceed to https://arounda-kb.firebaseapp.com and login with your email and password`
        };

        return mailTransport.sendMail(mailOptions, err => {
            if (err) {
                res.send(400).send("email error");
            }

            res.status(200).send(`User ${record.displayName} was created`);
        });
    };
};

const editUser = (req, res) => {
    const data = req.body;
    const uid = req.query.id;

    admin
        .auth()
        .getUser(uid)
        .then(record => {
            if (record.email !== data.email) {
                checkIfEmailIsUsed();
            } else {
                updateUser();
            }
        })
        .catch(() => {
            res.status(400).send("No user with such id exists");
        });

    const checkIfEmailIsUsed = () => {
        admin
            .auth()
            .getUserByEmail(data.email)
            .then(() => res.status(400).send("this email is already in use"))
            .catch(() => updateUser());
    };

    const updateUser = () => {
        const newData = {
            displayName: data.name,
            email: data.email
        };

        if (data.password && data.password.trim().length >= 6) {
            newData.password = data.password;
        }

        admin
            .auth()
            .updateUser(uid, newData)
            .then(() => {
                updateUserDoc();
            });
    };

    const updateUserDoc = () => {
        admin
            .firestore()
            .collection("users")
            .doc(uid)
            .update({
                name: data.name,
                email: data.email
            })
            .then(() => res.status(200).send(`user ${data.name} was updated`))
            .catch(() => {
                res.status(200).send("Cannot update profile doc. Try again");
            });
    };
};

const deleteUser = (req, res) => {
    const uid = req.query.id;

    const deleteUserDoc = () => {
        admin
            .firestore()
            .collection("users")
            .doc(uid)
            .delete()
            .then(() => {
                res.status(200).send("User was deleted");
            });
    };

    admin
        .auth()
        .deleteUser(uid)
        .then(() => deleteUserDoc())
        .catch(() => res.status(404).send("User was not found"));
};

module.exports = {
    createUser,
    editUser,
    deleteUser
};
