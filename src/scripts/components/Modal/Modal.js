import React, {Component} from "react";
import PropTypes from "prop-types";

class Modal extends Component {
    constructor(props) {
        super(props);
    }

    toggleModal(e) {
        if(e.target.classList.contains("modal_wrap")) {
            this.props.toggleModal();
        }
    }

    render() {
        return(
            <div className="modal_wrap modal_wrap-active" onClick={(e) => this.toggleModal(e)}>
                {this.props.children}
            </div>
        );
    }
}

Modal.propTypes = {
    toggleModal: PropTypes.func.isRequired,
    children: PropTypes.element.isRequired
}

export default Modal;