import React, {Component} from "react";
import ClickOutHandler from "react-onclickout";
import PropTypes from "prop-types";

import Dots from "../../../assets/icons/dots-black.svg";

class UserGroupsRow extends Component {
    constructor(props) {
        super(props);
        this.changeState = this.changeState.bind(this);

        this.state = {
            actionMenuOpen: false
        };
    }

    changeState(prop) {
        this.setState({
            [prop]: !this.state[prop]
        });
    }

    render() {
        const {title, id, read, edit, admin, changePermission} = this.props;
        return (
            <tr className="settings_section_table_row access-control_table_row">
                <td>{title}</td>
                <td>
                    <div className="checkbox-general">
                        <input
                            type="checkbox"
                            id={`canRead-${id}`}
                            name="canRead"
                            checked={read}
                            value={true}
                            onChange={() => changePermission(id, "read", !read)}
                        />
                        <label htmlFor={`canRead-${id}`}>
                            <span>reading</span>
                        </label>
                    </div>
                </td>
                <td>
                    <div className="checkbox-general">
                        <input
                            type="checkbox"
                            id={`canEdit-${id}`}
                            name="canEdit"
                            checked={edit}
                            value={true}
                            onChange={() => changePermission(id, "edit", !edit)}
                        />
                        <label htmlFor={`canEdit-${id}`}>
                            <span>editing</span>
                        </label>
                    </div>
                </td>
                <td>
                    <div className="checkbox-general">
                        <input
                            type="checkbox"
                            id={`canAdmin-${id}`}
                            name="canAdmin"
                            checked={admin}
                            value={true}
                            onChange={() => changePermission(id, "admin", !admin)}
                        />
                        <label htmlFor={`canAdmin-${id}`}>
                            <span>administration</span>
                        </label>
                    </div>
                </td>
                {this.props.title !== "Unassigned" && (
                    <td
                        className="u-mng_menu_trigger"
                        onClick={() => this.changeState("actionMenuOpen")}
                    >
                        <img className="u-mng_actions_icon" src={Dots} alt="user actions icon" />
                        {this.state.actionMenuOpen && (
                            <ClickOutHandler onClickOut={() => this.changeState("actionMenuOpen")}>
                                <ul
                                    className={`u-mng_menu_list ${
                                        this.state.actionMenuOpen ? "-open" : ""
                                    }`}
                                >
                                    <li onClick={() => this.props.handleEdit()}>Edit</li>
                                    <li onClick={() => this.props.handleDelete()}>Delete</li>
                                </ul>
                            </ClickOutHandler>
                        )}
                    </td>
                )}
            </tr>
        );
    }
}

UserGroupsRow.propTypes = {
    handleDelete: PropTypes.func.isRequired,
    handleEdit: PropTypes.func.isRequired,
    changePermission: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    read: PropTypes.bool,
    edit: PropTypes.bool,
    admin: PropTypes.bool
}

export default UserGroupsRow;
