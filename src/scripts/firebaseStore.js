import {createStore, combineReducers, compose} from "redux";
import {reactReduxFirebase, firebaseReducer} from "react-redux-firebase";
import {reduxFirestore, firestoreReducer} from "redux-firestore";
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";


const firebaseConfig = {
    apiKey: "AIzaSyCxrN49ItfQFf8YF0tEJa4zOL_kqSZHF-k",
    authDomain: "arounda-kb.firebaseapp.com",
    databaseURL: "https://arounda-kb.firebaseio.com",
    projectId: "arounda-kb",
    storageBucket: "arounda-kb.appspot.com",
};

const rrfConfig = {
    userProfile: "users",
    useFirestoreForProfile: true
};

firebase.initializeApp(firebaseConfig);
firebase.firestore();

const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig),
    reduxFirestore(firebase, rrfConfig)
)(createStore);

const rootReducer = combineReducers({
    firebase: firebaseReducer,
    firestore: firestoreReducer
});

const initialState = {};
const store = createStoreWithFirebase(rootReducer, initialState,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;