import React, {Component} from "react";
import {isLoaded} from "react-redux-firebase";
import ClickOutHandler from "react-onclickout";
import axios from "axios";
import PropTypes from "prop-types";


import InputGeneral from "../General/InputGeneral";
import {dataObjectToArray} from "../../tools/toolFunctions";
import StatusModal from "./StatusModal";

class CreateUser extends Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.onGroupChange = this.onGroupChange.bind(this);
        this.toggleSelect = this.toggleSelect.bind(this);
        this.validateFields = this.validateField.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.closeStatusModal = this.closeStatusModal.bind(this);

        this.state = {
            name: "",
            nameError: false,
            email: "",
            emailError: false,
            pass: "",
            passError: false,
            repeatPass: "",
            repeatPassError: false,
            groupId: "",
            groupIdError: false,
            groupSelectOpen: false,
            sendEmail: false,
            statusModalOpen: false,
            status: "idle"
        };
    }

    onInputChange(e, name, value) {
        const fieldName = name || e.target.name;
        const newValue = value || e.target.value;

        let newState = {};
        newState[fieldName] = newValue;

        this.setState(newState);
    }

    onGroupChange(id) {
        this.onInputChange(null, "groupId", id);
        this.toggleSelect();
    }

    toggleSelect() {
        this.setState({
            groupSelectOpen: !this.state.groupSelectOpen
        });
    }

    validateField(name) {
        switch (name) {
        case "name": {
            const name = this.state.name.trim();
            if (name.length === 0) {
                this.setState({
                    nameError: true
                });
            } else {
                this.setState({
                    nameError: false
                });
            }
            break;
        }
        case "email": {
            const email = this.state.email.trim();
            if (email.length === 0 || email.indexOf("@") === -1) {
                this.setState({
                    emailError: true
                });
            } else {
                this.setState({
                    emailError: false
                });
            }
            break;
        }
        case "pass": {
            const pass = this.state.pass.trim();
            if (pass.length === 0 || pass.length < 6) {
                this.setState({
                    passError: true
                });
            } else {
                this.setState({
                    passError: false
                });
            }
            break;
        }
        case "repeatPass": {
            const repeatPass = this.state.repeatPass.trim();
            if (repeatPass.length === 0 || repeatPass !== this.state.pass) {
                this.setState({
                    repeatPassError: true
                });
            } else {
                this.setState({
                    repeatPassError: false
                });
            }
            break;
        }
        case "groupId": {
            const groupId = this.state.groupId.trim();
            if (groupId.length === 0) {
                this.setState({
                    groupIdError: true
                });
            } else {
                this.setState({
                    groupIdError: false
                });
            }
        }
        }
    }

    validateForm() {
        const fieldNames = ["name", "email", "pass", "repeatPass"];

        let errorsFound = false;

        fieldNames.forEach(name => {
            if (this.state[`${name}Error`]) {
                errorsFound = true;
            }
        });

        if (this.state.groupId.trim().length === 0) {
            errorsFound = true;
            this.setState({
                groupIdError: true
            });
        } else {
            this.setState({
                groupIdError: false
            });
        }

        return errorsFound;
    }

    submitForm(e) {
        e.preventDefault();

        if (this.state.status === "creating") {
            return;
        }

        this.setState(
            {
                status: "creating",
                statusModalOpen: true
            },
            () => {
                const errorsFound = this.validateForm();
                if (errorsFound) {
                    return this.setState({
                        status: "idle"
                    });
                }

                const apiLink = `https://us-central1-arounda-kb.cloudfunctions.net/api/user?email=${
                    this.state.sendEmail
                }`;
                const data = {
                    name: this.state.name,
                    email: this.state.email,
                    password: this.state.pass,
                    groupId: this.state.groupId
                };

                this.props.firebase
                    .auth()
                    .currentUser.getIdToken()
                    .then(token => {
                        axios
                            .post(apiLink, data, {
                                headers: {"X-User-Token": token}
                            })
                            .then(() =>
                                this.setState({
                                    status: "success"
                                })
                            )
                            .catch(() => {
                                this.setState({
                                    status: "error"
                                });
                            });
                    });
            }
        );
    }

    closeStatusModal() {
        this.setState({
            status: "idle",
            statusModalOpen: false
        });
    }

    render() {
        if (!isLoaded(this.props.userGroups)) {
            return (
                <section className="settings_section">
                    <div className="settings_section_head">
                        <h1 className="settings_section_head_title">Add a user</h1>
                    </div>
                    Loading
                </section>
            );
        }

        let groupId = this.state.groupId;

        return (
            <section className="settings_section">
                <div className="settings_section_head">
                    <h1 className="settings_section_head_title">Add a user</h1>
                </div>
                <form className="settings_section_form" onSubmit={e => this.submitForm(e)}>
                    <InputGeneral
                        settings={{
                            placeholder: "User name",
                            name: "name",
                            required: true,
                            onBlurFunc: () => this.validateField("name")
                        }}
                        valueState={this.state.name}
                        errorState={this.state.nameError}
                        errorText="Name is not valid"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <InputGeneral
                        settings={{
                            placeholder: "Email",
                            type: "email",
                            name: "email",
                            required: true,
                            onBlurFunc: () => this.validateField("email")
                        }}
                        valueState={this.state.email}
                        errorState={this.state.emailError}
                        errorText="Email is already used"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <InputGeneral
                        settings={{
                            placeholder: "Password. 6 symbols min",
                            type: "password",
                            name: "pass",
                            required: true,
                            onBlurFunc: () => this.validateField("pass")
                        }}
                        valueState={this.state.pass}
                        errorState={this.state.passError}
                        errorText="Password is not valid"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <InputGeneral
                        settings={{
                            placeholder: "Confirm password",
                            type: "password",
                            name: "repeatPass",
                            required: true,
                            onBlurFunc: () => this.validateField("repeatPass")
                        }}
                        valueState={this.state.repeatPass}
                        errorState={this.state.repeatPassError}
                        errorText="Passwords are not the same"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />

                    <div className="create-user_select_wrap">
                        <p
                            className={`create-user_select_value ${
                                groupId.length > 0 ? "-filled" : "-empty"
                            }`}
                            onClick={() => this.toggleSelect()}
                        >
                            {groupId.length > 0
                                ? this.props.userGroups[groupId].title
                                : "Select a user group"}
                        </p>
                        {this.state.groupSelectOpen && (
                            <ClickOutHandler onClickOut={() => this.toggleSelect()}>
                                <ul className="create-user_select_list -open">
                                    {dataObjectToArray(this.props.userGroups).map(group => (
                                        <li
                                            key={group.id}
                                            onClick={() => this.onGroupChange(group.id)}
                                        >
                                            {group.title}
                                        </li>
                                    ))}
                                </ul>
                            </ClickOutHandler>
                        )}
                    </div>
                    <div className="checkbox-general create-user_checkbox">
                        <input
                            type="checkbox"
                            id="sendEmail"
                            name="sendEmail"
                            checked={this.state.sendEmail}
                            onChange={() => this.setState({sendEmail: !this.state.sendEmail})}
                        />
                        <label htmlFor="sendEmail">
                            <span>Send e-mail notification</span>
                        </label>
                    </div>
                    <button
                        type="submit"
                        className="btn-general btn-confirm-visible create-user_btn-submit"
                    >
                        <p>Save</p>
                    </button>
                </form>
                {this.state.statusModalOpen && (
                    <StatusModal
                        visible={this.state.statusModalOpen}
                        toggleModal={this.closeStatusModal}
                        status={this.state.status}
                    />
                )}
            </section>
        );
    }
}

CreateUser.propTypes = {
    firebase: PropTypes.object.isRequired,
    userGroups: PropTypes.object
}

export default CreateUser;
