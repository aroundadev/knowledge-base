import React, {Component} from "react";
import {isLoaded} from "react-redux-firebase";
import axios from "axios";
import PropTypes from "prop-types";


import InputGeneral from "../General/InputGeneral";
import StatusModal from "./StatusModal";

class EditUser extends Component {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.validateFields = this.validateField.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.closeStatusModal = this.closeStatusModal.bind(this);

        this.state = {
            userId: this.props.match.params.id,
            name: "",
            nameError: false,
            email: "",
            emailError: false,
            pass: "",
            passError: false,
            repeatPass: "",
            repeatPassError: false,
            statusModalOpen: false,
            status: "idle"
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userToEdit !== this.props.userToEdit) {
            this.setState({
                name: nextProps.userToEdit.name,
                email: nextProps.userToEdit.email
            });
        }
    }

    onInputChange(e, name, value) {
        const fieldName = name || e.target.name;
        const newValue = value || e.target.value;

        let newState = {};
        newState[fieldName] = newValue;

        this.setState(newState);
    }

    validateField(name) {
        switch (name) {
        case "name": {
            const name = this.state.name.trim();
            if (name.length === 0) {
                this.setState({
                    nameError: true
                });
            } else {
                this.setState({
                    nameError: false
                });
            }
            break;
        }
        case "email": {
            const email = this.state.email.trim();
            if (email.length === 0 || email.indexOf("@") === -1) {
                this.setState({
                    emailError: true
                });
            } else {
                this.setState({
                    emailError: false
                });
            }
            break;
        }
        case "pass": {
            const pass = this.state.pass.trim();
            if (this.state.pass.trim().length > 0 && (pass.length === 0 || pass.length < 6)) {
                this.setState({
                    passError: true
                });
            } else {
                this.setState({
                    passError: false
                });
            }
            break;
        }
        case "repeatPass": {
            const repeatPass = this.state.repeatPass.trim();
            if (
                this.state.pass.trim().length > 0 &&
                    (repeatPass.length === 0 || repeatPass !== this.state.pass)
            ) {
                this.setState({
                    repeatPassError: true
                });
            } else {
                this.setState({
                    repeatPassError: false
                });
            }
            break;
        }
        }
    }

    validateForm() {
        const fieldNames = ["name", "email"];

        let errorsFound = false;

        fieldNames.forEach(name => {
            if (this.state[`${name}Error`]) {
                errorsFound = true;
            }
        });

        if (
            this.state.pass.trim().length > 0 &&
            (this.state.passError || this.state.repeatPassError)
        ) {
            errorsFound = true;
        }

        return errorsFound;
    }

    submitForm(e) {
        e.preventDefault();

        if (this.state.status === "editing") {
            return;
        }

        this.setState(
            {
                status: "editing",
                statusModalOpen: true
            },
            () => {
                const errorsFound = this.validateForm();
                if (errorsFound) {
                    return this.setState({
                        status: "idle",
                        statusModalOpen: false
                    });
                }

                const apiURL = `https://us-central1-arounda-kb.cloudfunctions.net/api/user?id=${
                    this.state.userId
                }`;

                const data = {
                    name: this.state.name,
                    email: this.state.email,
                    password: this.state.pass
                };

                this.props.firebase
                    .auth()
                    .currentUser.getIdToken()
                    .then(token => {
                        axios
                            .patch(apiURL, data, {
                                headers: {"X-User-Token": token}
                            })
                            .then(() =>
                                this.setState({
                                    status: "success"
                                })
                            )
                            .catch(()=> {
                                this.setState({
                                    status: "error"
                                });
                            });
                    });
            }
        );
    }

    closeStatusModal() {
        this.setState({
            status: "idle",
            statusModalOpen: false
        });
    }

    render() {
        if (!isLoaded(this.props.userToEdit)) {
            return (
                <section className="settings_section">
                    <div className="settings_section_head">
                        <h1 className="settings_section_head_title">Edit profile</h1>
                    </div>
                    Loading
                </section>
            );
        }

        const user = this.props.userToEdit;

        return (
            <section className="settings_section">
                <div className="settings_section_head">
                    <h1 className="settings_section_head_title">Edit profile {user.name}</h1>
                </div>
                <form className="settings_section_form" onSubmit={e => this.submitForm(e)}>
                    <InputGeneral
                        settings={{
                            placeholder: "New user name",
                            name: "name",
                            required: true,
                            onBlurFunc: () => this.validateField("name")
                        }}
                        valueState={this.state.name}
                        errorState={this.state.nameError}
                        errorText="Name is not valid"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <InputGeneral
                        settings={{
                            placeholder: "New email",
                            type: "email",
                            name: "email",
                            required: true,
                            onBlurFunc: () => this.validateField("email")
                        }}
                        valueState={this.state.email}
                        errorState={this.state.emailError}
                        errorText="Email is already used"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <InputGeneral
                        settings={{
                            placeholder: "New password. 6 symbols min",
                            type: "password",
                            name: "pass",
                            onBlurFunc: () => {
                                this.validateField("pass");
                                this.validateField("repeatPass");
                            }
                        }}
                        valueState={this.state.pass}
                        errorState={this.state.passError}
                        errorText="Password is not valid"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />
                    <InputGeneral
                        settings={{
                            placeholder: "Confirm new password",
                            type: "password",
                            name: "repeatPass",
                            onBlurFunc: () => this.validateField("repeatPass")
                        }}
                        valueState={this.state.repeatPass}
                        errorState={this.state.repeatPassError}
                        errorText="Passwords are not the same"
                        changeHandler={this.onInputChange}
                        addClasses="settings_input"
                    />

                    <button
                        type="submit"
                        className="btn-general btn-confirm-visible create-user_btn-submit"
                    >
                        <p>Save</p>
                    </button>
                </form>
                {this.state.statusModalOpen && (
                    <StatusModal
                        visible={this.state.statusModalOpen}
                        toggleModal={this.closeStatusModal}
                        status={this.state.status}
                    />
                )}
            </section>
        );
    }
}

EditUser.propTypes = {
    match: PropTypes.object.isRequired,
    userToEdit: PropTypes.object,
    firebase: PropTypes.object.isRequired
}

export default EditUser;
