import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {compose} from "redux";
import PropTypes from "prop-types";

import EditUser from "./EditUser.js";

const mapStateToProps = state => ({
    userToEdit: state.firestore.data.userToEdit && state.firestore.data.userToEdit
});

const EditUserContainer = compose(
    firestoreConnect(props => {
        const id = props.match.params.id;
        
        return [{collection: "users", doc: id, storeAs: "userToEdit"}];
    }),
    connect(mapStateToProps)
)(EditUser);

EditUserContainer.propTypes = {
    match: PropTypes.object.isRequired
}

export default EditUserContainer;
