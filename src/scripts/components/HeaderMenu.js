import React, {Component} from "react";
import {Link} from "react-router-dom";
import {compose} from "redux";
import {connect} from "react-redux";
import {firestoreConnect} from "react-redux-firebase";
import PropTypes from "prop-types";
import Cookies from "js-cookie";

const menuChevrone = require("../../assets/icons/menu_chevrone.svg");

class HeaderMenu extends Component {
    constructor(props) {
        super(props);
        this.clearCookiesAndLogout = this.clearCookiesAndLogout.bind(this);
    }

    clearCookiesAndLogout() {
        Cookies.remove("user");
        this.props.firebase.logout().then(() => this.context.router.history.replace("/"));
    }

    render() {
        let admin = this.props.admin;

        return (
            <div className="header_menu_wrap">
                <p className="header_menu_label">
                    {this.props.user.name || ""}
                    <img src={menuChevrone} />
                </p>
                <div className="header_menu">
                    <p className="header_menu_name">{this.props.user.name || ""}</p>
                    <p className="header_menu_email">{this.props.user.email || ""}</p>
                    <ul className="header_menu_list">
                        <li>
                            <Link to={`/settings/edit-profile/${this.props.uid}`}>
                                Edit Profile
                            </Link>
                        </li>
                        {admin && (
                            <li>
                                <Link to="/settings/user-management">Settings</Link>
                            </li>
                        )}
                        <li onClick={this.clearCookiesAndLogout}>Sign Out</li>
                    </ul>
                </div>
            </div>
        );
    }
}

HeaderMenu.contextTypes = {
    router: PropTypes.object.isRequired
};

HeaderMenu.propTypes = {
    firebase: PropTypes.object.isRequired,
    user: PropTypes.object,
    admin: PropTypes.bool,
    uid: PropTypes.string,
}

const mapStateToProps = state => ({
    user: state.firebase.profile,
    uid: state.firebase.auth.uid,
    admin:
        state.firestore.data.currentUserGroup &&
        state.firestore.data.currentUserGroup.permissions.admin
});

export default compose(firestoreConnect(), connect(mapStateToProps))(HeaderMenu);
