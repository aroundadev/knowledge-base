import React, {Fragment} from "react";
import PropTypes from "prop-types";

import Modal from "../Modal/Modal";

const DeleteUserModal = ({userId, userName, toggleModal, deleteUser, status}) => {
    let statusText = `Are you sure that you that you want to delete ${userName}?`;
    switch (status) {
    case "idle": {
        statusText = `Are you sure that you that you want to delete ${userName}?`;
        break;
    }
    case "deleting": {
        statusText = "The user is being deleted...";
        break;
    }
    case "success": {
        statusText = "The user is deleted!";
        break;
    }
    case "error": {
        statusText = "Something went wrong in the process!";
        break;
    }
    }


    return (
        <Modal toggleModal={() => toggleModal()}>
            <div className="modal">
                <h3 className="modal_title">
                    {statusText}
                </h3>
    
                {status === "idle" && (
                    <Fragment>
                        <button
                            type="button"
                            className="btn-general btn-cancel modal_btn"
                            onClick={() => toggleModal()}
                        >
                            <p>No</p>
                        </button>
                        <button
                            type="button"
                            className="btn-general btn-confirm modal_btn"
                            onClick={() => deleteUser(userId)}
                        >
                            <p>Yes</p>
                        </button>
                    </Fragment>
                )}
    
                {(status === "success" || status === "error") && (
                    <button
                        type="button"
                        className="btn-general btn-confirm-visible modal_btn"
                        onClick={() => toggleModal()}
                    >
                        <p>Ok</p>
                    </button>
                )}
            </div>
        </Modal>
    );
} 

DeleteUserModal.propTypes = {
    userId: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    toggleModal: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    status: PropTypes.oneOf(["idle", "deleting", "success", "error"]).isRequired
}

export default DeleteUserModal;
